# Mort subite du nourrisson et plateforme d’échange

BAZINE Nohaila, KHOUITI Karima et SABRI Ouahiba

## Introduction

La Mort Subite du Nourrisson (MSN) est le décès subit d'un enfant âgé de 1 mois à 1 an jusqu'alors bien portant, sans qu'aucun antécédent connu ni événement récent ne puisse le prévoir. C’est la première cause de décès chez les enfants âgés de 28 jours à 1 an. Chaque année, 250 à 350 enfants de moins de deux ans décèdent de manière brutale en France, selon Santé Publique France.

## Objectif

Ce projet vise à analyser les témoignages concernant la MSN collectés le 30 mai 2024 sur le forum Doctissimo pour identifier des causes peu ou pas connues de ce phénomène en utilisant des techniques de traitement du langage naturel. Nous avons appliqué une série d'étapes de prétraitement et d'analyse avancée pour extraire des informations pertinentes et identifier des motifs communs dans les témoignages et discussions des utilisateurs.

## Partie 1 : Web Scraping

Cette étape consiste à extraire les discussions/témoignages sur la MSN depuis le forum Doctissimo.

**Bibliothèques utilisées** : `requests`, `BeautifulSoup4`, `pandas`

### Étapes du Web Scraping

1.  **Requête initiale** : Envoi d'une requête HTTP GET à la page de liste des sujets sur le forum Doctissimo pour récupérer le contenu de la page.
2.  **Extraction des liens des sujets** : Utilisation de BeautifulSoup pour parser le contenu HTML et extraire les URLs des différents sujets de discussion.
3.  **Formation correcte des URLs** : Assurer que toutes les URLs sont correctement formées pour être utilisées dans des requêtes ultérieures.
4.  **Scraping des sujets** : Pour chaque sujet, envoi d'une requête GET pour récupérer le contenu et extraction des informations pertinentes : auteur, la date et le contenu des posts.

**Résultat du Web Scraping**: - Récupération des informations pertinentes : auteur, date, et contenu des posts. - Les données ont été scrappées à partir de 773 discussions et témoignages publiés sur le forum Doctissimo concernant les causes et les expériences liées à la MSN.

## Partie 2 : Nettoyage des données

Cette étape comprend le nettoyage des données brutes extraites des témoignages.

**Bibliothèque utilisée** : `pandas`

### Étapes du nettoyage

1.  **Chargement des données** : Chargement des données depuis le fichier CSV créé lors de l'étape de scraping.
2.  **Gestion des valeurs manquantes** : Suppression des lignes entièrement vides et remplissage des valeurs manquantes pour certaines colonnes.
3.  **Formatage des dates** : Conversion des chaînes de caractères en objets de date/heure pour les manipulations futures.
4.  **Normalisation des textes** : Suppression des espaces en début et fin de chaîne dans les colonnes pertinentes.
5.  **Sauvegarde des données nettoyées** : Enregistrement des données nettoyées dans un nouveau fichier CSV.

## Partie 3 : Prétraitement standard des données

Cette étape applique des méthodes standards de prétraitement pour préparer les données à l'analyse NLP.

**Bibliothèques utilisées** : `nltk`, `spacy`, `pandas`

### Étapes du prétraitement

1.  **Normalisation des textes** : Mise en minuscule et suppression des caractères spéciaux dans les textes.
2.  **Tokenisation** : Découpage des textes en tokens individuels.
3.  **Suppression des stop words** : Suppression des mots vides (stop words) pour concentrer l'analyse sur les mots significatifs.
4.  **Lemmatisation** : Réduction des mots à leur forme de base (lemmatisation).
5.  **Stemming** : Réduction des mots à leur racine (stemming).
6.  **Application des étapes de prétraitement** : Application des fonctions de prétraitement à la colonne de texte des témoignages.
7.  **Sauvegarde des données prétraitées** : Enregistrement des données prétraitées dans un fichier CSV.

## Partie 4 : Analyse des fréquences et thématisation

**Analyse de fréquence** Nous avons compté les occurrences des mots lemmatisés pour identifier les termes les plus souvent associés aux témoignages sur la MSN.

**Bibliothèques utilisées** : `pandas`, `matplotlib`, `scikit-learn`, `wordcloud`, `gensim`

### Étapes de l'analyse

1.  **Mots clés fréquents** : Identification des termes les plus souvent associés aux témoignages sur la MSN.
2.  **Visualisation des données** : Création de graphiques en barres et de nuages de mots pour illustrer la fréquence des termes.
3.  **Clustering K-Means** : Regroupement des discussions en clusters pour identifier des thèmes récurrents.
4.  **Modélisation de sujets avec LDA (Latent Dirichlet Allocation)** : Entraînement du modèle LDA avec un nombre de sujets et extraction des mots importants pour chaque sujet identifié.

## Partie 5 : Résultats

**Bibliothèques utilisées** : `pandas`, `matplotlib`, `seaborn`

### Étapes des résultats

1.  **Graphiques de fréquence** : Visualisation des mots les plus influents dans chaque sujet identifié par l'analyse NLP.
2.  **Nuages de mots** : Visualisation des termes les plus fréquemment utilisés dans les clusters.
3.  **Thèmes identifiés** : Les principaux thèmes identifiés comprennent les pratiques de sommeil, les facteurs environnementaux, et les conditions médicales.

## Conclusion

Cette étude a utilisé des techniques de NLP pour analyser les discussions sur les causes de la Mort Subite du Nourrisson (MSN) sur les forums de Doctissimo. Les résultats montrent que les termes les plus fréquemment mentionnés sont "sommeil", "position", "allaitement", "température", "air", "respiration" et "tabagisme". Cela suggère que les utilisateurs discutent principalement de la manière dont les conditions de sommeil et la position de sommeil peuvent influencer le risque de MSN. De plus, les discussions incluent des préoccupations concernant la surchauffe, l’impact de la pollution de l'air, et la respiration des nourrissons ainsi que de l'impact du tabagisme. Ces discussions mettent en lumière les causes principales de la MSN selon les parents.
