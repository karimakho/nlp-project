
# Packages

import pandas as pd

# Data management

# Data
file_path = '/Users/bazinenohaila/Desktop/doctissimonlpnaima.csv'
df = pd.read_csv(file_path)

print("Aperçu des données avant nettoyage :")
print(df.head())

# Gestion des valeurs manquantes
df.dropna(how='all', inplace=True)

# Remplacement des valeurs manquantes dans la colonne 'Author' par 'Unknown'
if 'Author' in df.columns:
   df['Author'] = df['Author'].fillna('Unknown')

# Remplacement des valeurs manquantes dans la colonne 'Content' par une chaîne vide
if 'Content' in df.columns:
   df['Content'] = df['Content'].fillna('')


# 2. Formatage des dates
# Conversion des dates
if 'Date' in df.columns:
   def try_convert_date(date_str):
       try:
           return pd.to_datetime(date_str)
       except Exception:
           return date_str


   df['Date'] = df['Date'].apply(try_convert_date)


# 3. Normalisation des textes
# Suppression des espaces en début et fin de chaîne dans les colonnes 'Title' et 'Author'
if 'Title' in df.columns:
   df['Title'] = df['Title'].str.strip()
if 'Author' in df.columns:
   df['Author'] = df['Author'].str.strip()


# 4. Suppression des doublons
# Suppression des lignes en double
df.drop_duplicates(inplace=True)


# 5. Vérification et nettoyage des URL
# Supprimer les lignes avec des URL non valides
if 'URL' in df.columns:
   df = df[df['URL'].str.startswith('http')]

print("Aperçu des données après nettoyage :")
print(df.head())


# Données nettoyées dans un nouveau fichier CSV
cleaned_file_path = '/Users/bazinenohaila/Desktop/doctissimonlpnaima_cleaned.csv'
df.to_csv(cleaned_file_path, index=False)


print(f"Données nettoyées sauvegardées dans : {cleaned_file_path}")