
# Packages

import pandas as pd
import re
from nltk.corpus import stopwords
from nltk.tokenize import word_tokenize
from nltk.stem import WordNetLemmatizer, PorterStemmer
from nltk import sent_tokenize

# Chemin d'entrée du fichier CSV
input_path = '/Users/bazinenohaila/Desktop/doctissimonlpnaima_cleaned_bis.csv'

# Chemin de sortie pour le fichier CSV modifié
output_path = '/Users/bazinenohaila/Desktop/pos_content.csv'

# Charger les données depuis le fichier CSV fourni
df = pd.read_csv(input_path, encoding='utf-8')

print("Aperçu des données avant nettoyage :")
print(df.head())

# Fonction de normalisation (mise en minuscule et suppression des caractères spéciaux)
def normalize(text):
   text = text.lower()
   text = re.sub(r'\W', ' ', text)
   text = re.sub(r'\s+', ' ', text)
   return text

# Fonction de tokenisation
def tokenize(text):
   tokens = word_tokenize(text)
   return tokens

# Fonction de suppression des stop words
def remove_stopwords(tokens):
   stop_words = set(stopwords.words('french'))  # Adapter pour d'autres langues
   filtered_tokens = [word for word in tokens if word not in stop_words]
   return filtered_tokens

# Fonction de lemmatisation
def lemmatize(tokens):
   lemmatizer = WordNetLemmatizer()
   lemmatized_tokens = [lemmatizer.lemmatize(token) for token in tokens]
   return lemmatized_tokens

# Fonction de stemming
def stem(tokens):
   stemmer = PorterStemmer()
   stemmed_tokens = [stemmer.stem(token) for token in tokens]
   return stemmed_tokens

# Fonction simple pour attribuer des étiquettes PoS de manière basique
def simple_pos_tag(text):
   words = text.split()
   tags = []
   for word in words:
       if re.match(r'\b(?:est|suis|es|êtes|sommes|sont|était|étais|étaient|être)\b', word, re.I):
           tags.append((word, 'VB'))  # Verbe
       elif re.match(r'\b(?:un|une|le|la|les|des|du|de|d’|l’)\b', word, re.I):
           tags.append((word, 'DT'))  # Déterminant
       elif re.match(r'\b(?:il|elle|ils|elles|nous|je|tu|vous)\b', word, re.I):
           tags.append((word, 'PRP'))  # Pronom
       elif re.match(r'\b(?:et|ou|mais|si|parce que|comme|jusqu’à|pendant que)\b', word, re.I):
           tags.append((word, 'CC'))  # Conjonction
       elif re.match(r'\b(?:rapidement|silencieusement|bien|mal)\b', word, re.I):
           tags.append((word, 'RB'))  # Adverbe
       elif re.match(r'\b(?:beau|grand|petit|vert|rapide)\b', word, re.I):
           tags.append((word, 'JJ'))  # Adjectif
       else:
           tags.append((word, 'NN'))  # Nom (par défaut)
   return tags

column_to_clean = 'Content'  # La colonne contenant le texte à nettoyer
df[column_to_clean] = df[column_to_clean].astype(str)

df['normalized'] = df[column_to_clean].apply(normalize)
df['tokenized'] = df['normalized'].apply(tokenize)
df['no_stopwords'] = df['tokenized'].apply(remove_stopwords)
df['lemmatized'] = df['no_stopwords'].apply(lemmatize)
df['stemmed'] = df['lemmatized'].apply(stem)

# Etiquetage PoS a la colonne 'Content'
df['PoS_Tags'] = df['Content'].apply(lambda x: simple_pos_tag(str(x)))

print("Aperçu des données après nettoyage et étiquetage PoS :")
print(df[['Content', 'PoS_Tags']].head())

# DataFrame avec les etiquettes PoS dans un nouveau fichier CSV
df.to_csv(output_path, index=False)

print(f"Données nettoyées et étiquetées sauvegardées dans : {output_path}")