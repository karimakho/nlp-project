
# Packages

import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.cluster import KMeans

# Data
data_path = '/Users/bazinenohaila/Desktop/pos_content.csv'

df = pd.read_csv(data_path)

# Liste des termes du theme mort subite du nourisson
separated_terms = [
   "position", "sommeil", "surface", "surchauffe", "tabagisme", "parental",
   "naissance", "prématurée", "antécédents", "familiaux", "absence", "allaitement",
   "dysfonctionnement", "cardiaque", "anomalies", "cérébrales", "infections",
   "carences", "nutriments", "facteurs", "génétiques", "système", "immunitaire",
   "métaboliques", "perturbateurs", "endocriniens", "interruption", "chaîne",
   "respiratoire", "socio-économiques", "dormir", "même", "lit", "pollution",
   "air", "intérieur", "inversion", "température", "corporelle", "défauts",
   "respiration", "oxygénation", "malformations", "voies", "respiratoires",
   "toxines", "environnementales", "médicaments", "traumatismes", "crâniens",
   "hypoglycémie"
]

# Conversion de la liste des termes en minuscules
separated_terms = [term.lower() for term in separated_terms]

# Fonction pour rechercher les termes séparés dans la colonne lemmatisée
def search_separated_terms(df, terms):
   results = {}
   for term in terms:
       df['term_found'] = df['lemmatized'].apply(lambda x: term in x)
       results[term] = df[df['term_found'] == True]
   return results

# Rechercher les termes séparés
separated_search_results = search_separated_terms(df, separated_terms)

# Résumé des résultats de recherche séparés
separated_summary = {term: len(result) for term, result in separated_search_results.items()}

# Conversion le résumé en DataFrame
summary_df = pd.DataFrame(list(separated_summary.items()), columns=["Term", "Occurrences"])

# On filtre les termes avec moins de 2 occurrences
threshold = 2
filtered_summary_df = summary_df[summary_df['Occurrences'] >= threshold]

# Graphique à barres des occurrences filtrées
plt.figure(figsize=(14, 8))
sns.barplot(x="Occurrences", y="Term", hue="Term", data=filtered_summary_df.sort_values(by="Occurrences", ascending=False), palette="viridis", dodge=False)
plt.legend([],[], frameon=False)
plt.title('Occurrences of Terms in Lemmatized Text (Filtered)')
plt.xlabel('Occurrences')
plt.ylabel('Terms')
plt.show()

# Cluster K-means

# Chemins d'entrée et de sortie
file_path = '/Users/bazinenohaila/Desktop/pos_content_clustered.csv'
output_path = '/Users/bazinenohaila/Desktop/pos_content_clustered_with_analysis.csv'

# Data
data = pd.read_csv(file_path)

print("Colonnes disponibles :", data.columns)
colonne_texte = 'Content'
if colonne_texte not in data.columns:
   raise KeyError(f"La colonne {colonne_texte} n'existe pas dans le fichier CSV.")

# Valeurs manquantes avec des chaînes vides
data[colonne_texte] = data[colonne_texte].fillna("")

# Liste des termes séparés en mots individuels pour une recherche plus granulaire
separated_terms = [
   "position", "sommeil", "surface", "surchauffe", "tabagisme", "parental",
   "naissance", "prématurée", "antécédents", "familiaux", "absence", "allaitement",
   "dysfonctionnement", "cardiaque", "anomalies", "cérébrales", "infections",
   "carences", "nutriments", "facteurs", "génétiques", "système", "immunitaire",
   "métaboliques", "perturbateurs", "endocriniens", "interruption", "chaîne",
   "respiratoire", "socio-économiques", "dormir", "même", "lit", "pollution",
   "air", "intérieur", "inversion", "température", "corporelle", "défauts",
   "respiration", "oxygénation", "malformations", "voies", "respiratoires",
   "toxines", "environnementales", "médicaments", "traumatismes", "crâniens",
   "hypoglycémie"
]

# Conversion de la liste des termes en minuscules
separated_terms = [term.lower() for term in separated_terms]

# Vectorisation TF-IDF en utilisant seulement les termes spécifiques
vectorizer = TfidfVectorizer(vocabulary=separated_terms)
X = vectorizer.fit_transform(data[colonne_texte])

# Clustering avec K-Means : fonction
num_clusters = 5  # Ajustez selon vos besoins
km = KMeans(n_clusters=num_clusters, random_state=42)
km.fit(X)
clusters = km.labels_.tolist()

# Ajout des clusters au dataframe
data['cluster'] = clusters

# Analyse des mots-clés de chaque cluster et leur regroupement pour le graphique en barres empilées
terms = vectorizer.get_feature_names_out()
order_centroids = km.cluster_centers_.argsort()[:, ::-1]

# Initialisation d'un DataFrame pour les fréquences des mots par cluster
freq_df = pd.DataFrame(index=terms)

for i in range(num_clusters):
   cluster_data = data[data['cluster'] == i]
   X = vectorizer.transform(cluster_data[colonne_texte])
   sum_words = X.sum(axis=0)
   words_freq = [(word, sum_words[0, idx]) for word, idx in vectorizer.vocabulary_.items()]
   words_freq = sorted(words_freq, key=lambda x: x[1], reverse=True)

   # DataFrame pour les fréquences des mots de ce cluster
   cluster_df = pd.DataFrame(words_freq, columns=['Term', f'Cluster_{i}']).set_index('Term')

   freq_df = freq_df.join(cluster_df, how='left')

# Remplacement des NaN par des zéros
freq_df = freq_df.fillna(0)

freq_df['Total'] = freq_df.sum(axis=1)
freq_df = freq_df.sort_values('Total', ascending=False).drop(columns='Total')

# Sélection des termes les plus fréquents pour le graphique
top_n_terms = 10
top_terms_df = freq_df.head(top_n_terms)

# Graphique en barres empilées
top_terms_df.plot(kind='bar', stacked=True, figsize=(14, 8), colormap='viridis')
plt.title('Top Terms in Clusters')
plt.xlabel('Terms')
plt.ylabel('Occurrences')
plt.xticks(rotation=45)
plt.legend(title='Cluster')
plt.tight_layout()
plt.show()

data.to_csv(output_path, index=False)


# Nuage de mots


from wordcloud import WordCloud

# Nuage de mots pour chaque cluster
for i in range(num_clusters):
    cluster_data = data[data['cluster'] == i]
    text = " ".join(content for content in cluster_data[colonne_texte])

    wordcloud = WordCloud(width=800, height=400, background_color='white').generate(text)

    plt.figure(figsize=(10, 6))
    plt.imshow(wordcloud, interpolation='bilinear')
    plt.axis('off')
    plt.title(f'Word Cloud for Cluster {i}')
    plt.show()

# LDA : Latent dirichlet allocation


# Topic Modeling avec LDA
lda = LatentDirichletAllocation(n_components=num_clusters, max_iter=10, learning_method='online', learning_decay=0.7,
                               random_state=42)
lda.fit(X)

# Préparation des données pour le graphique à barres empilées
top_n_words = 10
topics = lda.components_

# Collection des données des mots les plus importants pour chaque sujet
topic_words = []
for i, topic in enumerate(topics):
   top_indices = topic.argsort()[-top_n_words:]
   top_words = [vectorizer.get_feature_names_out()[index] for index in top_indices]
   top_words_values = topic[top_indices]
   topic_words.append((top_words, top_words_values))

# DataFrame pour les mots de chaque sujet
words = []
values = []
topics_list = []

for i, (top_words, top_words_values) in enumerate(topic_words):
   words.extend(top_words)
   values.extend(top_words_values)
   topics_list.extend([f'Topic {i}'] * top_n_words)

df_topics = pd.DataFrame({
   'Word': words,
   'Value': values,
   'Topic': topics_list
})