

# Packages

import requests
from bs4 import BeautifulSoup
import pandas as pd
import time

# URL de la page de liste des sujets
list_url = "https://forum.doctissimo.fr/grossesse-bebe/mort-subite-nourrisson/liste_sujet-1.htm"
# En-têtes pour imitation d'un navigateur web
headers = {
    'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.3'
}

# Requête GET pour obtention du contenu de la page
for _ in range(3):  # Tentatives
    response = requests.get(list_url, headers=headers)
    if response.status_code == 200:
        print("Successfully fetched the page content")
        break
    else:
        print(f"Failed to fetch the page content: {response.status_code}. Retrying...")
        time.sleep(5)  # Attente de 5 secondes avant réessai
else:
    print("Failed to fetch the page content after multiple attempts.")
    raise SystemExit

soup = BeautifulSoup(response.content, "html.parser")

# Extraction des URLs des sujets
topic_links = soup.find_all("a", class_="cCatTopic")
if topic_links:
    print(f"Found {len(topic_links)} topic links")
else:
    print("No topic links found")
    raise SystemExit

# Formation correcte des URLs
topic_urls = []
for link in topic_links:
    href = link["href"]
    if not href.startswith("http"):
        href = "https://forum.doctissimo.fr" + href
    topic_urls.append(href)

# Liste de stockage des données
data = []

# Fonction de scraping d'un sujet
def scrape_topic(url):
    response = requests.get(url, headers=headers)
    if response.status_code == 200:
        print(f"Successfully fetched the topic content: {url}")
    else:
        print(f"Failed to fetch the topic content: {response.status_code} - {url}")
        return

    soup = BeautifulSoup(response.content, "html.parser")

# Extraction des posts (auteurs et contenu)
    posts = soup.find_all("div", class_="md-post__content-body")
    if not posts:
        print(f"No posts found for topic: {url}")

    for post in posts:
        try:
            author = post.find_previous("span", itemprop="author").get_text(strip=True)
            date = post.find_previous("meta", itemprop="dateCreated")["content"]
            content = post.get_text(strip=True)
            data.append({"Author": author, "Date": date, "Content": content})
        except AttributeError as e:
            print(f"Error parsing post: {e}")

# Scraping de chaque sujet
for url in topic_urls:
    scrape_topic(url)

# Vérification de la collecte des données
if data:
    print(f"Collected data for {len(data)} posts")
else:
    print("No data collected")

# Création d'un DataFrame pandas
df = pd.DataFrame(data)

# Enregistrement des données dans un fichier CSV local
csv_file_path = '/karimakhouiti/Desktop/doctissimo_data.csv'
df.to_csv(csv_file_path, index=False)

print(f"Data has been saved to {csv_file_path}")